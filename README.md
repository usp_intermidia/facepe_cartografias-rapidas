# FACEPE_cartografias-rapidas

O projeto Cartografias Rápidas, em parceria com o SENAI-PE, financiado pela FACEPE, visa trabalhar no monitoramento e mitigação de Desastres Naturais nas áreas de riscos localizadas na cidade de Recife. 

No primeiro momento, o desenvolvimento das tecnologias empregadas no projeto estão direcionadas ao acompanhamento de Movimentação de Massas em Encostas, precisamente em comunicardes carentes. Nesta primeira etapa, o projeto elencou como prioridade a comunidade denominada Córrego da Bica. Onde estão sendo realizados implantações e testes de sensores digitais, com processamento de imagens satelitais / drones. 

Este repositório irá contar com alguns dos programas desenvolvidos ao decorrer do projeto. Trazendo a implementação de uma rede MESH em modo LoRA. Onde cada _node_ tem como papel ser um coletar e transmissor, finaliando a cadeia de transmissão com o _gateway_. 