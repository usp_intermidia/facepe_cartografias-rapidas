// teste dht11

#include "DHT.h"

#define DHTPIN 4
#define DHTTYPE DHT11

//


// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, routed reliable messaging client
// with the RHMesh class.
// It is designed to work with the other examples bridge
// Hint: you can simulate other network topologies by setting the 
// RH_TEST_NETWORK define in RHRouter.h

// Mesh has much greater memory requirements, and you may need to limit the
// max message length to prevent weird crashes
#define RH_MESH_MAX_MESSAGE_LEN 50

#include <RHMesh.h>
#include <RH_RF95.h>
#include <SPI.h>

// In this small artifical network of 4 nodes,
#define BRIDGE_ADDRESS 764  // address of the bridge ( we send our data to, hopefully the bridge knows what to do with our data )
#define NODE_ADDRESS 1    // address of this node

// lilygo T3 v2.1.6
// lora SX1276/8
  #define LLG_SCK 5
  #define LLG_MISO 19
  #define LLG_MOSI 27
  #define LLG_CS  18
  #define LLG_RST 23
  #define LLG_DI0 26
  #define LLG_DI1 33
  #define LLG_DI2 32

#define TXINTERVAL 5000  // delay between successive transmissions

#define SERIAL_DEBUG_BAUD 115200
#define uS_TO_S_FACTOR 1000000  // Fator de conversão de micro segundos para segundos
#define TIME_TO_SLEEP 30       // tempo de sleep em segundos

unsigned long nextTxTime;

// Singleton instance of the radio driver
RH_RF95 rf95(LLG_CS, LLG_DI0); // slave select pin and interrupt pin, [heltec|ttgo] ESP32 Lora OLED with sx1276/8

// Class to manage message delivery and receipt, using the driver declared above
RHMesh manager(rf95, NODE_ADDRESS);

//dht11
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(SERIAL_DEBUG_BAUD);

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);  // tempo deepSleep
  Serial.println("----- Acordei ^_º -----");

  Serial.print(F("initializing node "));
  Serial.print(NODE_ADDRESS);
  SPI.begin(LLG_SCK,LLG_MISO,LLG_MOSI,LLG_CS);
  if (!manager.init())
    Serial.println(" init failed"); 
  else
    Serial.println(" done");  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36 

  rf95.setTxPower(5, false); // with false output is on PA_BOOST, power from 2 to 20 dBm, use this setting for high power demos/real usage
  // rf95.setTxPower(0, true); // true output is on RFO, power from 0 to 15 dBm, use this setting for low power demos ( does not work on lilygo lora32 )
  rf95.setFrequency(433.0);
  rf95.setCADTimeout(500);

  // long range configuration requires for on-air time
  boolean longRange = false;
  if (longRange) {
    // custom configuration
    RH_RF95::ModemConfig modem_config = {
      0x1D, // Reg 0x1D: BW=125kHz, Coding=4/8, Header=explicit
      0x1E, // Reg 0x1E: Spread=4096chips/symbol, CRC=enable
      0x26  // Reg 0x26: LowDataRate=On, Agc=Off.  0x0C is LowDataRate=ON, ACG=ON
      };
    rf95.setModemRegisters(&modem_config);
  } else{
    // Predefined configurations( bandwidth, coding rate, spread factor ):
    // Bw125Cr45Sf128     Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range
    // Bw500Cr45Sf128     Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range
    // Bw31_25Cr48Sf512   Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range
    // Bw125Cr48Sf4096    Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, low data rate, CRC on. Slow+long range
    // Bw125Cr45Sf2048    Bw = 125 kHz, Cr = 4/5, Sf = 2048chips/symbol, CRC on. Slow+long range
    if (!rf95.setModemConfig(RH_RF95::Bw500Cr45Sf128))
      Serial.println(F("set config failed"));
  }
  Serial.println("RF95 ready");
  nextTxTime = millis();


  //dht11
  dht.begin();

  collectData();

  Serial.print(" ----- Vou dormir -_- -----");
  Serial.flush();
  esp_deep_sleep_start();
}

void loop() {}

// uint8_t data[] = "Hello World!";
// Dont put this on the stack:

// For node collection
uint8_t res;

// for node bridge
uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];
uint8_t from;

void collectData() {
  float umidade = random(0, 100);
  float temperatura = random(0, 50);

  // float umidade = 0;
  // float temperatura = 0;
  // dht11(umidade ,temperatura); // run dht11 - remove alfter tests

  String buffer;
  buffer = umidade;
  buffer += ";";
  buffer += temperatura;
  char data[buffer.length() + 1];
  buffer.toCharArray(data, sizeof(data));

  // nextTxTime += TXINTERVAL;
  Serial.print("Sending to bridge n.");
  Serial.print(BRIDGE_ADDRESS);
  Serial.print(" res=");
  
  // Send a message to a rf95_mesh_server
  // A route to the destination will be automatically discovered.
  res = manager.sendtoWait((uint8_t*)data, sizeof(data), BRIDGE_ADDRESS);
  Serial.print(res);
  Serial.println(" - " + buffer);

  if (res == RH_ROUTER_ERROR_NONE) // Data has been reliably delivered to the next node. Now we do...
    Serial.println("Success send data"); 
  else // Data not delivered to the next node.
    Serial.println("sendtoWait failed. Are the bridge/intermediate mesh nodes running?");
}

void dht11(float &umidade, float &temperatura) {
  // Serial.println();
  umidade = dht.readHumidity();
  temperatura = dht.readTemperature();
  if (isnan(umidade) || isnan(temperatura)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  // Serial.print(F("Humidity: "));
  // Serial.print(umidade);
  // Serial.print(F("%  Temperature: "));
  // Serial.print(temperatura);
  // Serial.print(F("°C "));
  // Serial.println();
}