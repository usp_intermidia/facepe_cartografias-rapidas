#define R2 100
#define R3 100

#define VOLTAGE_MAX 4200
#define VOLTAGE_MIN 3300
#define ADC_REFERENCE 2405

#define VOLTAGE_OUT(Vin) (((Vin)*R3) / (R2 + R3))

#define VOLTAGE_TO_ADC(in) ((ADC_REFERENCE * (in)) / 4096)
#define BATTERY_MAX_ADC VOLTAGE_TO_ADC(VOLTAGE_OUT(VOLTAGE_MAX))
#define BATTERY_MIN_ADC VOLTAGE_TO_ADC(VOLTAGE_OUT(VOLTAGE_MIN))


class Bateria {

  // ---------------- Atributos ----------------
private:
  float bateryLevel = 0.0;
  unsigned int raw = 0;
  int vbatPin = A13;                     //Pin Default
  float batteryCapacity = 2405.0f;  //Capacidade da Bateria
  void setup();
  // -------------------------------------------

public:
  Bateria();
  int getRawBatteryValue();
  float getPercentageBatteryValue();
  int getPin();
  void setPin(int sPin);
  int getBatteryCapacity();
  void setBatteryCapacity(int sBatteryCapacity);
  float calc_battery_percentage();
  void toString();
};

Bateria::Bateria() {
  setup();
}

void Bateria::setup() {
  pinMode(vbatPin, INPUT);
}


int Bateria::getRawBatteryValue() {
  return analogRead(vbatPin);  // Deve-se multiplicar por 2 por causa do divisor de tensão.
}

float Bateria::getPercentageBatteryValue() {
  raw = getRawBatteryValue();  
  float percent = map(raw, 0.0f, batteryCapacity, 0, 100);
  Serial.printf("Battery raw: %d - %.2f\%\n", raw, percent);
  return percent;
}

float Bateria::calc_battery_percentage() {
  float battery_voltage = analogReadMilliVolts(vbatPin);
  battery_voltage *= 0.002;    // we divided by 2, so multiply back
  float battery_percentage = map(battery_voltage, 3.2f, 4.2f, 0, 100);
  Serial.printf("Battery raw: %d - %.2f v - %.2f\%\n", raw, battery_voltage, battery_percentage);
  if (battery_percentage < 0)
    battery_percentage = 0;
  if (battery_percentage > 100)
    battery_percentage = 100;
  return battery_percentage;
}


int Bateria::getPin() {
  return vbatPin;
}

void Bateria::setPin(int sPin) {
  vbatPin = sPin;
}

int Bateria::getBatteryCapacity() {
  return batteryCapacity;
}

void Bateria::setBatteryCapacity(int sBatteryCapacity) {
  batteryCapacity = sBatteryCapacity;
}