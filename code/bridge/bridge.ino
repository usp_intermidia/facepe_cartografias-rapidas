#include "lib/ThingsBoardWrapper.h"
#include "lib/WifiClientWrapper.h"

// Begin - WiFi config
#include <WiFi.h>
#include <ESP32Ping.h>

#define SSID "comglutem"
#define PASSWORD "0LActose.."

String get_wifi_status(int status){
    switch(status){
        case WL_IDLE_STATUS:
        return "WL_IDLE_STATUS";
        case WL_SCAN_COMPLETED:
        return "WL_SCAN_COMPLETED";
        case WL_NO_SSID_AVAIL:
        return "WL_NO_SSID_AVAIL";
        case WL_CONNECT_FAILED:
        return "WL_CONNECT_FAILED";
        case WL_CONNECTION_LOST:
        return "WL_CONNECTION_LOST";
        case WL_CONNECTED:
        return "WL_CONNECTED";
        case WL_DISCONNECTED:
        return "WL_DISCONNECTED";
    }
}
// End - WiFi config


// bridge_polite
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, routed reliable messaging client
// with the RHMesh class.
// It is designed to work with the other examples rf95_mesh_server*
// Hint: you can simulate other network topologies by setting the 
// RH_TEST_NETWORK define in RHRouter.h

// Mesh has much greater memory requirements, and you may need to limit the
// max message length to prevent wierd crashes
#define RH_MESH_MAX_MESSAGE_LEN 50

// RadioHead
#include <RHMesh.h>
#include <RH_RF95.h>
#include <SPI.h>

// In this small artifical network of 4 nodes,
#define BRIDGE_ADDRESS 764  // address of the bridge ( we send our data to, hopefully the bridge knows what to do with our data )

// lilygo T3 v2.1.6
// lora SX1276/8
#define LLG_SCK 5
#define LLG_MISO 19
#define LLG_MOSI 27
#define LLG_CS  18
#define LLG_RST 23
#define LLG_DI0 26
#define LLG_DI1 33
#define LLG_DI2 32

#define RXTIMEOUT 1000  // it is roughly the delay between successive transmissions

char* ipPing = "www.google.com.br";
uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];

constexpr char CURRENT_FIRMWARE_TITLE[] = "est_met_firm";
constexpr char CURRENT_FIRMWARE_VERSION[] = "2.3.0";
constexpr char CURRENT_CLIENT_ID[] = "est_met_02";

const unsigned int resetWifiManagerPin = 34;
const unsigned long long bitMaskPin = 0x400000000;

// Singleton instance of the radio driver
RH_RF95 rf95(LLG_CS, LLG_DI0); // slave select pin and interrupt pin, [heltec|ttgo] ESP32 Lora OLED with sx1276/8

// Class to manage message delivery and receipt, using the driver declared above
RHMesh manager(rf95, BRIDGE_ADDRESS);

WifiClientWrapper wifiControl(resetWifiManagerPin, bitMaskPin);
ThingsBoardWrapper clientMQTT(CURRENT_FIRMWARE_TITLE, CURRENT_FIRMWARE_VERSION, CURRENT_CLIENT_ID);

void setup() {
  Serial.begin(115200);
  Serial.print(F("initializing node "));
  Serial.print(BRIDGE_ADDRESS);
  SPI.begin(LLG_SCK,LLG_MISO,LLG_MOSI,LLG_CS);
  if (!manager.init())
    Serial.println(" init failed");
  else
    Serial.println(" done");  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36 

  rf95.setTxPower(5, false); // with false output is on PA_BOOST, power from 2 to 20 dBm, use this setting for high power demos/real usage
  // rf95.setTxPower(15, true); // true output is on RFO, power from 0 to 15 dBm, use this setting for low power demos ( does not work on lilygo lora32 )
  rf95.setFrequency(433.0);
  rf95.setCADTimeout(500);

  // long range configuration requires for on-air time
  boolean longRange = false;
  if (longRange) {
    // custom configuration
    RH_RF95::ModemConfig modem_config = {
      0x1D, // Reg 0x1D: BW=125kHz, Coding=4/8, Header=explicit
      0x1E, // Reg 0x1E: Spread=4096chips/symbol, CRC=enable
      0x26  // Reg 0x26: LowDataRate=On, Agc=Off.  0x0C is LowDataRate=ON, ACG=ON
      };
    rf95.setModemRegisters(&modem_config);
    } else {
    // Predefined configurations( bandwidth, coding rate, spread factor ):
    // Bw125Cr45Sf128     Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range
    // Bw500Cr45Sf128     Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range
    // Bw31_25Cr48Sf512   Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range
    // Bw125Cr48Sf4096    Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, low data rate, CRC on. Slow+long range
    // Bw125Cr45Sf2048    Bw = 125 kHz, Cr = 4/5, Sf = 2048chips/symbol, CRC on. Slow+long range
    if (!rf95.setModemConfig(RH_RF95::Bw500Cr45Sf128))
      Serial.println(F("set config failed"));
    }
  Serial.println("RF95 ready");
}



void loop() {
  uint8_t len = sizeof(buf);
  uint8_t from;
  if (manager.recvfromAck(buf, &len, &from)) {
    
    String buffer = (char*)buf;
    String concat = "node:";
    concat += from;

    Serial.print("request data: ");
    Serial.print(concat);
    Serial.print(" ");
    Serial.print(buffer);
    Serial.print(" :rssi: ");
    Serial.println(rf95.lastRssi()); 

    if (!Ping.ping(ipPing, 5))
      wifiConnect(SSID, PASSWORD);
    if(wifiControl.connectWifi()) {
      clientMQTT.mqttServerURL = wifiControl.mqttServerURL;
      clientMQTT.mqttDeviceToken = "estacao-saocarlos";
      if (clientMQTT.connectToIoTServer()) {
        int delayMsg = 5; // 5 segundos
        bool debug   = false;
        // calculateAndSendTelemetryFromPluviometer(delayMsg);   
        // clientMQTT.sendTelemetryFloat("winSpeed", anemometro.getWinSpeed(), delayMsg, debug);
        // clientMQTT.sendTelemetryString("winPointer", anemoscopio.getDirectionAsCardialPoint(), delayMsg, debug);
        // clientMQTT.sendTelemetryFloat("battery", bateria.getPercentageBatteryValue(), delayMsg, debug);
        // clientMQTT.sendTelemetryFloat("battery_2", bateria.calc_battery_percentage(), delayMsg, debug);     
        clientMQTT.sendTelemetryString(concat.c_str(), buffer.c_str(), delayMsg, debug);

        // clientMQTT.firmwareUpdate(notJustUpdated);
        // notJustUpdated = 1;
      }
    }
  }
}

void wifiConnect(char* ssid, char* password) {
  delay(50);
  int status = WL_IDLE_STATUS;
  Serial.println("\nConnecting");
  Serial.println(get_wifi_status(status));
  WiFi.begin(ssid, password);
  while(status != WL_CONNECTED){
      delay(500);
      status = WiFi.status();
      Serial.println(get_wifi_status(status));
  }
  Serial.println("\nConnected to the WiFi network");
  Serial.print("Local ESP32 IP: ");
  Serial.println(WiFi.localIP());
}